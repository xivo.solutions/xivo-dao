# -*- coding: utf-8 -*-

# Copyright (C) 2012-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import logging

from xivo_dao.alchemy.userpreferences import UserPreferences
from xivo_dao.helpers.db_manager import daosession

logger = logging.getLogger(__name__)


def get_user_preference(user_id):
    return {_.key: _.value for _ in get(user_id)}


@daosession
def get(session, user_id):
    return session.query(UserPreferences).filter(UserPreferences.user_id == int(user_id)).all()
