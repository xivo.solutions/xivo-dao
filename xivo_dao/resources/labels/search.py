# -*- coding: UTF-8 -*-

# Copyright (C) 2020 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from xivo_dao.alchemy.labels import Labels
from xivo_dao.resources.utils.search import SearchSystem
from xivo_dao.resources.utils.searchers import SearchConfig

config = SearchConfig(table=Labels,
                      columns={'id': Labels.id,
                               'display_name': Labels.display_name},
                      search=['display_name'],
                      default_sort='display_name')

label_search = SearchSystem(config)
