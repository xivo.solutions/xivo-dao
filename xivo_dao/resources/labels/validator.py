# -*- coding: utf-8 -*-

# Copyright (C) 2020 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from xivo_dao.helpers import errors
from xivo_dao.resources.labels import dao


def validate_create(label_display_name):
    _validate_exists_already(label_display_name)


def _validate_exists_already(label_display_name):
    existing_label = _find_by_name(label_display_name)
    if existing_label:
        raise errors.resource_exists('Label', name=label_display_name)


def _find_by_name(label_display_name):
    return dao.get_by_display_name(label_display_name)
