# -*- coding: utf-8 -*-

# Copyright (C) 2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


from xivo_dao.alchemy.rightcall import RightCall as CallPermission
from xivo_dao.resources.utils.search import SearchSystem
from xivo_dao.resources.utils.searchers import SearchConfig

config = SearchConfig(table=CallPermission,
                      columns={'id': CallPermission.id,
                               'name': CallPermission.name,
                               'description': CallPermission.description,
                               'enabled': CallPermission.enabled,
                               'mode': CallPermission.mode},
                      default_sort='name')

call_permission_search = SearchSystem(config)
