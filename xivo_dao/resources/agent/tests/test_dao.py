# -*- coding: UTF-8 -*-

# Copyright (C) 2015 Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from hamcrest import assert_that, equal_to, contains_inanyorder

from xivo_dao import queue_member_dao
from xivo_dao.alchemy.agentfeatures import AgentFeatures
from xivo_dao.alchemy.queuefeatures import QueueFeatures
from xivo_dao.resources.agent import dao as agent_dao
from xivo_dao.resources.utils.search import SearchResult
from xivo_dao.tests.test_dao import DAOTestCase


class TestAgentExist(DAOTestCase):

    def test_given_no_agent_then_returns_false(self):
        result = agent_dao.exists(1)

        assert_that(result, equal_to(False))

    def test_given_agent_exists_then_return_true(self):
        agent_row = self.add_agent()

        result = agent_dao.exists(agent_row.id)

        assert_that(result, equal_to(True))


class BaseTestAgentDAO(DAOTestCase):
    def add_agent(self, **kwargs):
        kwargs.setdefault('numgroup', 1)
        kwargs.setdefault('passwd', '')
        kwargs.setdefault('context', 'default')
        kwargs.setdefault('language', '')
        kwargs.setdefault('description', '')

        agent = AgentFeatures(**kwargs)

        self.session.add(agent)
        self.session.flush()
        return agent


class TestSearch(BaseTestAgentDAO):

    def assert_search_returns_result(self, search_result, **parameters):
        result = agent_dao.search(**parameters)
        assert_that(result, equal_to(search_result))

    def test_empty_result(self):
        expected = SearchResult(0, [])

        self.assert_search_returns_result(expected)

    def test_return_all(self):
        agent1 = self.add_agent(firstname='bob', number='2001')
        agent2 = self.add_agent(firstname='sully', number='2002')

        expected = SearchResult(2, [agent1, agent2])

        self.assert_search_returns_result(expected)

    def test_return_match(self):
        agent1 = self.add_agent(firstname='bob', number='2001')
        agent2 = self.add_agent(firstname='sully', number='2002')

        expected = SearchResult(1, [agent2])

        self.assert_search_returns_result(expected, firstname='sully')


class TestAgentCRUD(BaseTestAgentDAO):
    def test_get_agent(self):
        expected = self.add_agent(firstname='James', lastname='Bond', number='2002')
        agent = agent_dao.get(expected.id)
        assert_that(agent, equal_to(expected))

    def test_create_agent(self):
        props = {
            'firstname': 'Jack',
            'lastname': 'Bauer',
            'number': '2003',
            'numgroup': 1,
            'passwd': '',
            'context': 'default',
            'language': 'en_US',
            'description': 'An agent'
        }
        agent = agent_dao.create(AgentFeatures(**props))
        expected = agent_dao.get(agent.id)
        assert_that(agent, equal_to(expected))

    def test_edit_agent(self):
        agent = self.add_agent(firstname='James', lastname='Bond', number='2002')
        agent.firstname = 'Secret'
        agent.lastname = 'Agent'
        agent_dao.edit(agent)
        result = agent_dao.get(agent.id)
        assert_that(agent, equal_to(result))


class TestAgentMembership(BaseTestAgentDAO):
    def test_get_agent_membership(self):
        agent_id = 123
        agent_number = '123'
        queue1 = QueueFeatures(name='queue1', displayname='queue 1')
        queue2 = QueueFeatures(name='queue2', displayname='queue 2')
        self.session.add(queue1)
        self.session.add(queue2)
        self.session.flush()

        queue_member_dao.add_agent_to_queue(agent_id, agent_number, queue1.name)
        queue_member_dao.add_agent_to_queue(agent_id, agent_number, queue2.name)

        res = agent_dao.get_membership(123)

        member1 = {'agent_id': 123, 'queue_id': queue1.id, 'penalty': 0}
        member2 = {'agent_id': 123, 'queue_id': queue2.id, 'penalty': 0}

        assert_that(res, contains_inanyorder(member1, member2))
