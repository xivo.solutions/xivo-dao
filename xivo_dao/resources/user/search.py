# -*- coding: utf-8 -*-

# Copyright (C) 2014-2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import logging

from sqlalchemy.sql import and_
from sqlalchemy.sql.functions import concat

from xivo_dao.alchemy.agentfeatures import AgentFeatures
from xivo_dao.alchemy.entity import Entity
from xivo_dao.alchemy.extension import Extension
from xivo_dao.alchemy.linefeatures import LineFeatures
from xivo_dao.alchemy.user_line import UserLine
from xivo_dao.alchemy.userfeatures import UserFeatures
from xivo_dao.alchemy.usersip import UserSIP
from xivo_dao.alchemy.voicemail import Voicemail
from xivo_dao.resources.utils.search import SearchSystem, SearchSystemNG
from xivo_dao.resources.utils.searchers import SearchConfig

logger = logging.getLogger(__name__)

columns = {
    'id': UserFeatures.id,
    'uuid': UserFeatures.uuid,
    'firstname': UserFeatures.firstname,
    'lastname': UserFeatures.lastname,
    'fullname': concat(UserFeatures.firstname, " ", UserFeatures.lastname),
    'caller_id': UserFeatures.callerid,
    'description': UserFeatures.description,
    'userfield': UserFeatures.userfield,
    'email': UserFeatures.email,
    'mobile_phone_number': UserFeatures.mobilephonenumber,
    'music_on_hold': UserFeatures.musiconhold,
    'outgoing_caller_id': UserFeatures.outcallerid,
    'preprocess_subroutine': UserFeatures.preprocess_subroutine,
    'entity': Entity.name,
    'voicemail_number': Voicemail.mailbox,
    'provisioning_code': LineFeatures.provisioning_code,
    'exten': Extension.exten,
    'extension': Extension.exten,
    'context': Extension.context,
    'enabled': UserFeatures.enabled,
    'agentid': UserFeatures.agentid
}


def get_configuration():
    configuration = {
        'table': UserFeatures,
        'columns': columns,
        'search': [
            'fullname',
            'caller_id',
            'description',
            'userfield',
            'email',
            'mobile_phone_number',
            'music_on_hold',
            'preprocess_subroutine',
            'outgoing_caller_id',
            'exten',
            'provisioning_code',
            'enabled'
        ],
        'default_sort': 'lastname'
    }
    return configuration


class UserSearchSystem(SearchSystem):

    def search_from_query(self, query, parameters=None):
        query = self.search_on_extension(query)
        return super(UserSearchSystem, self).search_from_query(query, parameters)

    @staticmethod
    def search_on_extension(query):
        return (query
                .outerjoin(Entity,
                           UserFeatures.entity_id == Entity.id)
                .outerjoin(UserLine,
                           UserLine.user_id == UserFeatures.id)
                .outerjoin(LineFeatures,
                           and_(LineFeatures.id == UserLine.line_id,
                                LineFeatures.commented == 0))
                .outerjoin(Extension,
                           and_(UserLine.extension_id == Extension.id,
                                Extension.commented == 0))
                .outerjoin(Voicemail,
                           and_(UserFeatures.voicemailid == Voicemail.uniqueid,
                                Voicemail.commented == 0))
                .outerjoin(AgentFeatures,
                           UserFeatures.agentid == AgentFeatures.id)
                .outerjoin(UserSIP,
                           and_(LineFeatures.protocol == 'sip',
                                LineFeatures.protocolid == UserSIP.id)))


user_search = UserSearchSystem(SearchConfig(**get_configuration()))


def get_configuration_ng():
    configuration = {
        'table': UserFeatures,
        'columns': columns,
        'search': [
            'exten',
            'fullname',
            'userfield',
            'email',
            'description',
            'mobile_phone_number',
            'voicemail_number'
        ],
        'default_sort': 'lastname',
        'similarity_threshold': 0.1,
        'unique_column': 'id'
    }
    return configuration


class UserSearchSystemNG(UserSearchSystem, SearchSystemNG):

    def list_from_query(self, query, unique_ids):
        query = self.search_on_extension(query)
        return super(UserSearchSystem, self).list_from_query(query, unique_ids)


user_search_ng = UserSearchSystemNG(SearchConfig(**get_configuration_ng()))
