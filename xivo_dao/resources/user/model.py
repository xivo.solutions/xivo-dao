# -*- coding: utf-8 -*-

# Copyright (C) 2013-2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


class UserModel(object):
    def __init__(self, id, uuid, firstname, lastname, email, timezone, language, description,
                 caller_id, outgoing_caller_id, mobile_phone_number, username, password,
                 music_on_hold, preprocess_subroutine, userfield,
                 call_transfer_enabled, call_record_enabled, online_call_record_enabled, supervision_enabled,
                 ring_seconds, simultaneous_calls, call_permission_password, agent_number, agentid, agent_group_id,
                 enabled, labels):
        self.id = id
        self.uuid = uuid
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.timezone = timezone
        self.language = language
        self.description = description
        self.caller_id = caller_id
        self.outgoing_caller_id = outgoing_caller_id
        self.mobile_phone_number = mobile_phone_number
        self.username = username
        self.password = password
        self.music_on_hold = music_on_hold
        self.preprocess_subroutine = preprocess_subroutine
        self.userfield = userfield
        self.call_transfer_enabled = call_transfer_enabled
        self.call_record_enabled = call_record_enabled
        self.online_call_record_enabled = online_call_record_enabled
        self.supervision_enabled = supervision_enabled
        self.ring_seconds = ring_seconds
        self.simultaneous_calls = simultaneous_calls
        self.call_permission_password = call_permission_password
        self.agent_number = agent_number
        self.agentid = agentid
        self.agent_group_id = agent_group_id
        self.enabled = enabled
        self.labels = labels

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class UserDirectory(object):

    def __init__(self, id, uuid, line_id, agent_id, firstname, lastname, exten, email,
                 mobile_phone_number, voicemail_number, userfield, description, context):
        self.id = id
        self.uuid = uuid
        self.line_id = line_id
        self.agent_id = agent_id
        self.firstname = firstname
        self.lastname = lastname
        self.exten = exten
        self.email = email
        self.mobile_phone_number = mobile_phone_number
        self.voicemail_number = voicemail_number
        self.userfield = userfield
        self.description = description
        self.context = context

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class UserSummary(object):

    def __init__(self, id, uuid, firstname, lastname, enabled, provisioning_code, protocol,
                 extension, context, entity, configregistrar, line_type):
        self.id = id
        self.uuid = uuid
        self.firstname = firstname
        self.lastname = lastname
        self.enabled = enabled
        self.provisioning_code = provisioning_code
        self.protocol = protocol
        self.extension = extension
        self.context = context
        self.entity = entity
        self.configregistrar = configregistrar
        self.line_type = line_type

    def __eq__(self, other):
        return self.__dict__ == other.__dict__
