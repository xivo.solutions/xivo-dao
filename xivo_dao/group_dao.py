# -*- coding: utf-8 -*-

# Copyright (C) 2012-2015 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from xivo_dao.alchemy.groupfeatures import GroupFeatures
from xivo_dao.alchemy.mediaserver import MediaServer
from xivo_dao.alchemy.queuemember import QueueMember
from xivo_dao.helpers.db_manager import daosession


@daosession
def get(session, group_id):
    return session.query(GroupFeatures).filter(GroupFeatures.id == group_id).first()


def get_name_number(group_id):
    group = get(group_id)
    return group.name, group.number


@daosession
def is_user_member_of_group(session, user_id, group_id):
    row = (session
           .query(GroupFeatures.id)
           .join((QueueMember, QueueMember.queue_name == GroupFeatures.name))
           .filter(GroupFeatures.id == group_id)
           .filter(QueueMember.usertype == 'user')
           .filter(QueueMember.userid == user_id)
           .first())
    return row is not None


@daosession
def all(session):
    return session.query(GroupFeatures).all()


@daosession
def get_mds_by_group_id(session, group_id):
    mds = (session.query(MediaServer.name)
           .join(GroupFeatures, MediaServer.id == GroupFeatures.mediaserverid)
           .filter(GroupFeatures.id == int(group_id))
           .scalar())
    if not mds:
        raise LookupError('Could not find a group with id %s', group_id)
    else:
        return mds
