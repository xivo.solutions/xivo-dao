# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy import ForeignKeyConstraint
from sqlalchemy.schema import Column, PrimaryKeyConstraint, UniqueConstraint
from sqlalchemy.types import Integer, String

from xivo_dao.helpers.db_manager import Base


class IvrEditNodeCond(Base):
    __tablename__ = 'ivredit_nodecon'
    __table_args__ = (
        PrimaryKeyConstraint('id'),
        ForeignKeyConstraint(('id_flowchart', 'src',),
                             ('ivr.ivredit_node.id_flowchart', 'ivr.ivredit_node.node_number',),
                             name='ivredit_nodecon_src_fkey', ondelete='CASCADE', ),
        ForeignKeyConstraint(('id_flowchart', 'dst',),
                             ('ivr.ivredit_node.id_flowchart', 'ivr.ivredit_node.node_number',),
                             name='ivredit_nodecon_dst_fkey', ondelete='CASCADE', ),
        UniqueConstraint('id_flowchart', 'src', 'src_opt'),
        {'schema': 'ivr'},
    )

    id = Column('id', Integer, autoincrement=True, nullable=False)
    id_flowchart = Column('id_flowchart', Integer, nullable=False)
    src = Column('src', Integer, nullable=False)
    src_opt = Column('src_opt', String(10), nullable=False)
    dst = Column('dst', Integer, nullable=False)
