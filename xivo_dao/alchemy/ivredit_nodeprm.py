# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy import CheckConstraint, column, ForeignKeyConstraint
from sqlalchemy.schema import Column, PrimaryKeyConstraint, UniqueConstraint
from sqlalchemy.types import Integer, String

from xivo_dao.helpers.db_manager import Base


class IvrEditNodePrm(Base):
    __tablename__ = 'ivredit_nodeprm'
    __table_args__ = (
        PrimaryKeyConstraint('id'),
        ForeignKeyConstraint(('id_node',), ('ivr.ivredit_node.id',), name='ivredit_nodeprm_id_fkey',
                             ondelete='CASCADE', ),
        UniqueConstraint('id_node', 'prm_name'),
        CheckConstraint(column('prm_name') != ''),
        {'schema': 'ivr'},
    )

    id = Column('id', Integer, autoincrement=True, nullable=False)
    id_node = Column('id_node', Integer, nullable=False)
    prm_name = Column('prm_name', String(50), nullable=False)
    prm_value = Column('prm_value', String(255), nullable=False)
