# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy import CheckConstraint, column, Text, Numeric
from sqlalchemy.schema import Column, PrimaryKeyConstraint, UniqueConstraint
from sqlalchemy.types import Integer, String

from xivo_dao.helpers.db_manager import Base


class IvrEditVoicePrompt(Base):
    __tablename__ = 'ivredit_voiceprompt'
    __table_args__ = (
        PrimaryKeyConstraint('id'),
        UniqueConstraint('name'),
        CheckConstraint(column('name') != ''),
        {'schema': 'ivr'},
    )

    id = Column('id', Integer, autoincrement=True, nullable=False)
    name = Column('name', String(50), nullable=False)
    source = Column('source', String(20), nullable=False)
    msg = Column('msg', Text)
    lang = Column('lang', String(2))
    speed = Column('speed', Numeric(2, 1), server_default='1.0')
