# -*- coding: utf-8 -*-

# Copyright (C) 2020 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.schema import Column
from sqlalchemy.types import Integer

from xivo_dao.helpers.db_manager import Base


class User_Label_Contact(Base):
    __tablename__ = 'user_label_contact'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('usercontact.id', ondelete='CASCADE'))
    label_id = Column(Integer, ForeignKey('label_contact.id', ondelete='CASCADE'))

    __table_args__ = (
        Index('label_contact__idx__label_id', 'label_id'),
        Index('usercontact__idx__user_id', 'user_id'),
    )

    usercontact = relationship("UserContact", backref=backref("user_label_contact"))
    label_contact = relationship("Label_Contact", backref=backref("user_label_contact"))
