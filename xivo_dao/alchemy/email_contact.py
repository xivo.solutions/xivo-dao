# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy.schema import Column, PrimaryKeyConstraint, Index, \
    UniqueConstraint, ForeignKeyConstraint
from sqlalchemy.types import Integer, String

from xivo_dao.helpers.db_manager import Base


class Email_Contact(Base):
    __tablename__ = 'email_contact'
    __table_args__ = (
        PrimaryKeyConstraint('email_id'),
        ForeignKeyConstraint(('usercontact_user_id',),
                             ('usercontact_user.id',),
                             ondelete='RESTRICT'),
        UniqueConstraint('id', name='email_id'),
        Index('email_contact__idx__email', 'email'),
        Index('email_contact__idx__user_id', 'user_id'),
    )

    email_id = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    email = Column(String(128), nullable=False)
    user_id = Column(Integer, nullable=False, autoincrement=False, Foreign_key=True)
