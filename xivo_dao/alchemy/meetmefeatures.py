# -*- coding: utf-8 -*-

# Copyright (C) 2007-2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy.schema import Column, PrimaryKeyConstraint, Index, UniqueConstraint
from sqlalchemy.types import Integer, String, Text, Enum

from xivo_dao.helpers.db_manager import Base


class MeetmeFeatures(Base):
    __tablename__ = 'meetmefeatures'
    __table_args__ = (
        PrimaryKeyConstraint('id'),
        UniqueConstraint('meetmeid'),
        Index('meetmefeatures__idx__context', 'context'),
        Index('meetmefeatures__idx__number', 'confno'),
    )

    id = Column(Integer, nullable=False, autoincrement=True)
    meetmeid = Column(Integer, nullable=False)
    name = Column(String(128), nullable=False)
    confno = Column(String(40), nullable=False)
    context = Column(String(39), nullable=False)

    announceusercount = Column(Integer, nullable=False, server_default='0')
    user_announcejoinleave = Column(Enum('no', 'yes', 'noreview',
                                         name='meetmefeatures_announcejoinleave',
                                         metadata=Base.metadata),
                                    nullable=False)
    musiconhold = Column(String(128))
    quiet = Column(Integer, nullable=False, server_default='0')
    record = Column(Integer, nullable=False, server_default='0')
    noplaymsgfirstenter = Column(Integer, nullable=False, server_default='0')
    maxusers = Column(Integer, nullable=False, server_default='0')
    preprocess_subroutine = Column(String(39))
    description = Column(Text, nullable=False)
    commented = Column(Integer, server_default='0')
    user_pin = Column(String(12))
    admin_pin = Column(String(12))
    user_waitingroom = Column(Integer, nullable=False, server_default='0')
