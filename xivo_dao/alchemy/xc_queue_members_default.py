# -*- coding: utf-8 -*-

# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy.orm import relationship
from sqlalchemy.schema import Column, PrimaryKeyConstraint, ForeignKeyConstraint
from sqlalchemy.types import Integer

from xivo_dao.alchemy.xc_queues import XCQueues
from xivo_dao.alchemy.xc_users import XCUsers
from xivo_dao.helpers.db_manager import Base


class XCQueueMembersDefault(Base):
    __tablename__ = 'queue_members_default'
    __table_args__ = (
        PrimaryKeyConstraint('user_id', 'queue_id', name='queue_members_default_pkey'),
        ForeignKeyConstraint(('user_id',),
                             ('xc.users.id',), ),
        ForeignKeyConstraint(('queue_id',),
                             ('xc.queues.id',), ),
        {'schema': 'xc'},
    )

    user_id = Column(Integer, nullable=False)
    queue_id = Column(Integer, nullable=False)
    penalty = Column(Integer, nullable=False, server_default='0')

    users = relationship(XCUsers)
    queues = relationship(XCQueues)
