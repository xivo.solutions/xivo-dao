# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy import CheckConstraint, column, ForeignKeyConstraint
from sqlalchemy.schema import Column, PrimaryKeyConstraint, UniqueConstraint
from sqlalchemy.types import Integer, String

from xivo_dao.helpers.db_manager import Base


class IvrEditNode(Base):
    __tablename__ = 'ivredit_node'
    __table_args__ = (
        PrimaryKeyConstraint('id'),
        ForeignKeyConstraint(('id_flowchart',), ('ivr.ivredit_flowchart.id',), name='ivredit_node_id_flowchart_id_fkey',
                             ondelete='CASCADE', ),
        UniqueConstraint('id_flowchart', 'node_number'),
        {'schema': 'ivr'},
    )

    id = Column(Integer, nullable=False, autoincrement=True)
    id_flowchart = Column('id_flowchart', Integer, nullable=False)
    node_number = Column('node_number', Integer, nullable=False)
    node_type = Column('node_type', String(20), nullable=False)
    name = Column('name', String(50), nullable=False)
