# -*- coding: utf-8 -*-

# Copyright (C) 2007-2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy import text
from sqlalchemy.schema import Column, PrimaryKeyConstraint
from sqlalchemy.types import Integer, String, Text, DateTime

from xivo_dao.helpers.db_manager import Base


class QualificationAnswers(Base):
    __tablename__ = 'qualification_answers'
    __table_args__ = (
        PrimaryKeyConstraint('id'),
    )

    id = Column(Integer, nullable=False, autoincrement=True)
    sub_qualification_id = Column(Integer)
    time = Column(DateTime, server_default=text("(current_timestamp at time zone 'utc')"))
    callid = Column(String(128), nullable=False)
    agent = Column(Integer)
    queue = Column(Integer)
    first_name = Column(String(128), nullable=True)
    last_name = Column(String(128), nullable=True)
    comment = Column(String(255), nullable=True)
    custom_data = Column(Text, nullable=True)
