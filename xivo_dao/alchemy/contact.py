# -*- coding: utf-8 -*-

# Copyright (C) 2020 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy.orm import relationship
from sqlalchemy.schema import Column, PrimaryKeyConstraint, Index, \
    UniqueConstraint
from sqlalchemy.types import Integer, String

from xivo_dao.helpers.db_manager import Base


class Contact(Base):
    __tablename__ = 'contact'
    __table_args__ = (
        PrimaryKeyConstraint('id'),
        UniqueConstraint('id', name='contact_id'),
        Index('contact__idx__firstname', 'firstname'),
        Index('contact__idx__lastname', 'lastname'),
    )

    id = Column(Integer, nullable=False, autoincrement=True, primary_key=True)
    firstname = Column(String(128), nullable=False, server_default='')
    lastname = Column(String(128), nullable=False, server_default='')
    address = Column(String(128), nullable=True)
    title = Column(String(128), nullable=True)
    company = Column(String(128), nullable=True)

