# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy.schema import Column, PrimaryKeyConstraint, ForeignKeyConstraint
from sqlalchemy.types import Integer

from xivo_dao.helpers.db_manager import Base


class RouteMediaServer(Base):
    __tablename__ = 'routemediaserver'
    __table_args__ = (
        PrimaryKeyConstraint('routeid', 'mdsid'),
        ForeignKeyConstraint(('routeid',),
                             ('route.id',),
                             ondelete='RESTRICT'),
        ForeignKeyConstraint(('mdsid',),
                             ('mediaserver.id',),
                             ondelete='RESTRICT'),
    )

    routeid = Column(Integer, nullable=False)
    mdsid = Column(Integer, nullable=False)
