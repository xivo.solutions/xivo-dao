# -*- coding: utf-8 -*-

# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy.orm import relationship
from sqlalchemy.schema import Column, PrimaryKeyConstraint, ForeignKeyConstraint
from sqlalchemy.types import Integer, Enum

from xivo_dao.alchemy.users import Users
from xivo_dao.helpers.db_manager import Base


class Rights(Base):
    __tablename__ = 'rights'
    __table_args__ = (
        PrimaryKeyConstraint('id'),
        ForeignKeyConstraint(('user_id',),
                             ('users.id',), ),
    )

    id = Column(Integer, nullable=False, autoincrement=True)
    user_id = Column(Integer, nullable=False)
    category = Column(Enum('queue', 'agentgroup', 'incall', 'recording_access', 'dissuasion_access',
                           name='right_type',
                           metadata=Base.metadata),
                      nullable=False)
    category_id = Column(Integer, nullable=False)

    users = relationship(Users)
