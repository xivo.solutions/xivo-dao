# -*- coding: utf-8 -*-

# Copyright (C) 2012-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from hamcrest import assert_that
from hamcrest import equal_to
from hamcrest import has_entries

from xivo_dao import userpreferences
from xivo_dao.tests.test_dao import DAOTestCase


class TestUserFeaturesDAO(DAOTestCase):
    def test_get_userpreferences(self):
        preferences = {
            'key1': 'value1',
            'key2': 'value2'
        }
        user_id = self._generate_int()
        self.add_user(id=user_id)
        for k, v in preferences.items():
            self.add_userpreference(user_id=user_id, key=k, value=v)

        obtained_prefs = userpreferences.get_user_preference(user_id)

        assert_that(obtained_prefs, has_entries(preferences))

    def test_get_userpreferences_empty(self):
        user_id = self._generate_int()
        obtained_prefs = userpreferences.get_user_preference(user_id)
        assert_that(obtained_prefs, equal_to({}))
