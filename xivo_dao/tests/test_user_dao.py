# -*- coding: utf-8 -*-

# Copyright (C) 2012-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


from hamcrest import assert_that
from hamcrest import equal_to

from xivo_dao import user_dao
from xivo_dao.helpers.exception import NotFoundError
from xivo_dao.tests.test_dao import DAOTestCase


class TestUserFeaturesDAO(DAOTestCase):

    def test_get_user_by_number_context(self):
        context, number = 'default', '1234'
        user_line = self.add_user_line_with_exten(exten=number,
                                                  context=context)

        user = user_dao.get_user_by_number_context(number, context)

        assert_that(user.id, equal_to(user_line.user.id))

    def test_get_user_by_number_context_line_commented(self):
        context, number = 'default', '1234'
        self.add_user_line_with_exten(exten=number,
                                      context=context,
                                      commented_line=1)

        self.assertRaises(LookupError, user_dao.get_user_by_number_context, number, context)

    def test_get_user_with_agent_notfound(self):
        self.assertRaises(NotFoundError, user_dao.get_user_with_agent_number, 42)

    def test_get_user_without_agent(self):
        user = self.add_user()

        res = user_dao.get_user_with_agent_number(user.id)

        assert_that(res.UserFeatures.id, equal_to(user.id))
        assert_that(res.UserFeatures.agentid, equal_to(None))
        assert_that(res.agent_number, equal_to(None))
        assert_that(res.agent_group_id, equal_to(None))

    def test_get_user_by_id_with_agent_number(self):
        agent = self.add_agent()
        user = self.add_user(agentid=agent.id)

        res = user_dao.get_user_with_agent_number(user.id)

        assert_that(res.UserFeatures.id, equal_to(user.id))
        assert_that(res.UserFeatures.agentid, equal_to(agent.id))
        assert_that(res.agent_number, equal_to(str(agent.number)))
        assert_that(res.agent_group_id, equal_to(agent.numgroup))

    def test_get_user_by_uuid_with_agent_number(self):
        agent = self.add_agent()
        user = self.add_user(agentid=agent.id)

        res = user_dao.get_user_with_agent_number(user.uuid)

        assert_that(res.UserFeatures.id, equal_to(user.id))
        assert_that(res.UserFeatures.agentid, equal_to(agent.id))
        assert_that(res.agent_number, equal_to(str(agent.number)))
        assert_that(res.agent_group_id, equal_to(agent.numgroup))

    def test_get_mds_by_user_id(self):
        user = self.add_user()
        line = self.add_line(configregistrar='mds1')
        self.add_user_line(user_id=user.id, line_id=line.id)

        res = user_dao.get_mds_by_user_id(user.id)

        assert_that(res, equal_to('mds1'))

    def test_get_user_by_username(self):
        username = "jbond"
        user = self.add_user(loginclient=username, firstname="James", lastname="Bonds")

        res = user_dao.get_user_by_username(username)

        assert_that(res, equal_to(user))
