# -*- coding: utf-8 -*-

# Copyright (C) 2012-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import logging

from sqlalchemy import and_

from xivo_dao.alchemy.agentfeatures import AgentFeatures
from xivo_dao.alchemy.extension import Extension as ExtensionSchema
from xivo_dao.alchemy.linefeatures import LineFeatures
from xivo_dao.alchemy.user_line import UserLine
from xivo_dao.alchemy.userfeatures import UserFeatures
from xivo_dao.helpers import errors
from xivo_dao.helpers.db_manager import daosession

logger = logging.getLogger(__name__)


@daosession
def get(session, user_id):
    result = session.query(UserFeatures).filter(UserFeatures.id == int(user_id)).first()
    if result is None:
        raise LookupError()
    return result



@daosession
def get_user_by_username(session, username):
    result = session.query(UserFeatures).filter(UserFeatures.loginclient == str(username)).first()
    if result is None:
        raise LookupError()
    return result


@daosession
def get_user_by_number_context(session, exten, context):
    user = (session.query(UserFeatures)
            .join(ExtensionSchema, and_(ExtensionSchema.context == context,
                                        ExtensionSchema.exten == exten,
                                        ExtensionSchema.commented == 0))
            .join(LineFeatures, and_(LineFeatures.commented == 0))
            .join(UserLine, and_(UserLine.user_id == UserFeatures.id,
                                 UserLine.extension_id == ExtensionSchema.id,
                                 UserLine.line_id == LineFeatures.id,
                                 UserLine.main_line == True,
                                 UserLine.main_line == True))
            .first())

    if not user:
        raise LookupError('No user with number %s in context %s', (exten, context))

    return user


def filter_user_by_id_uuid(query, id):
    if isinstance(id, int):
        return query.filter(UserFeatures.id == id)
    else:
        id = str(id)
        if id.isdigit():
            return query.filter(UserFeatures.id == int(id))
        else:
            return query.filter(UserFeatures.uuid == id)


@daosession
def get_user_with_agent_number(session, user_id):
    users_with_agent = (session.query(
        UserFeatures,
        AgentFeatures.number.label('agent_number'),
        AgentFeatures.numgroup.label('agent_group_id'))
        .outerjoin(
        AgentFeatures, UserFeatures.agentid == AgentFeatures.id))

    result = (filter_user_by_id_uuid(users_with_agent, user_id)
              .first())

    if result is None:
        raise errors.not_found('User', id=user_id)

    return result


@daosession
def get_mds_by_user_id(session, user_id):
    mds = (session.query(LineFeatures.configregistrar)
           .join(UserLine, and_(UserLine.line_id == LineFeatures.id,
                                UserLine.user_id == int(user_id),
                                UserLine.main_user == True,
                                UserLine.main_line == True))
           .scalar())
    if not mds:
        raise LookupError('Could not find a line for user %s', user_id)
    else:
        return mds
