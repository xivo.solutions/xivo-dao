FROM python:3.11-slim-bookworm

RUN apt -yqq update \
    && apt -yqq install \
       python3-pip \
       git \
       libpq-dev \
       python3-dev \
       libyaml-dev

WORKDIR /usr/src/dao

ADD requirements.txt .
ADD test-requirements.txt .
RUN pip install -r requirements.txt
RUN pip install -r test-requirements.txt

ADD . /usr/src/dao

CMD python3 -m unittest
