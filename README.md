# xivo-dao

xivo-dao is a library used internally by XiVO to access and modify different data sources (e.g. postgres database,
provisoning database).

## Database Evolution

xivo-dao is responsible for table creation.

See [private database wiki page](https://gitlab.com/avencall/randd-doc/wikis/coding/database) for a more high level
overview.

When you **add** a table you have to:

- *create* a `mynewtable.py` file in the in `xivo_dao/alchemy` folder
- in this `mynewtable.py` file you must describe the table in SqlAlchemy format
- and then add an import of this table in the file `xivo_dao/alchemy/all/__init__.py`.

The list must include all classes from `xivo_dao/alchemy` except of `enum`.

When you **modify** a table you have to:

- *edit* table description in the `myexistingtable.py` file in the in `xivo_dao/alchemy` folder
- and if relevant modify the import of this table in the file `xivo_dao/alchemy/all/__init__.py`.

When you **delete** a table you have to:

- *remove* table description in the `myexistingtable.py` file in the in `xivo_dao/alchemy` folder
- and remove the import of this table in the file `xivo_dao/alchemy/all/__init__.py`.

**All addition/modification must also be done in an alembic evolution script** in xivo.solutions/xivo-db> project (for
this addition/modification be taken into account during xivo upgrade).

## Running unit tests

### Via Docker Compose (recommended way)

To run with docker-compose:

    docker compose up -d db  # only once
    # wait a bit
    docker compose build dao && docker compose up dao  # repeat to run against new code

Note if you want to clean your test env :

    docker compose down

### Other historical way of running unit tests

#### (DEPRECATED) Via Docker

You need the test database ``asterisktest`` installed somewhere (see below).

To run test with docker:

    docker build -t xivo/dao-test .
    docker run -e XIVO_TEST_DB_URL=<postgres_uri> -it xivo/dao bash

#### (DEPRECATED) Via your system

**1. Create the test database**

```
apt-get install postgres-9.4
sudo -u postgres psql
```

Then:

```
CREATE DATABASE asterisktest;
CREATE USER asterisk WITH PASSWORD 'asterisk';
GRANT ALL ON DATABASE asterisktest TO asterisk;
CREATE SCHEMA xc AUTHORIZATION asterisk;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "unaccent";
```

**2. Launching the tests**

You need the test database ``asterisktest`` installed (see above).

```
apt-get install libpq-dev python-dev libyaml-dev
pip install tox
tox --recreate -e py27
```

To execute tests slightly faster, you can avoid recreating all the tables in the database by
passing ```CREATE_TABLES=0``` on the command line.

## Testing Database evolution in dev

Check [xivo-db readme](https://gitlab.com/xivo.solutions/xivo-db/-/blob/master/README.md#testing-database-evolution-in-dev)
to have way to test with your local XiVO.